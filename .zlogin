alias sudo='sudo ' # enable aliases to work w/ sudo
alias ssu='sudo su -'

alias l='ls -lFh'
alias la='ls -laFh'
alias dud='du -d 1 -h' # sizes of dirs
alias duf='du -sh *' # sizes of files
alias mkdir='mkdir -p' # necessary to overwrite systemadmin plugin alias
alias j-tail='sudo journalctl -f -u ' # simpler tailing of systemd services

if (( $+commands[ccat] )); then
  alias cat='ccat'
fi

if (( $+commands[consul-cli] )); then
  if [[ -S "/opt/consul/socket/consul.sock" ]]; then
    alias cci='consul-cli --consul="unix:///opt/consul/socket/consul.sock"'
  else
    alias cci='consul-cli'
  fi
fi

# Editors
alias svim='sudo vim'
if (( $+commands[nvim] )); then
  # Set default editor to nvim
  export EDITOR='nvim'
  # Setup aliases
  alias n='nvim'
  alias sn='sudo nvim'
else # use vim, simpler than remembering neovim availability
  # Set default editor to vim
  export EDITOR='vim'
  alias n='vim'
  alias sn='sudo vim'
fi

# Ensure zsh completion tools are loaded
autoload -U +X bashcompinit && bashcompinit

# Nomad autocompletion
if (( $+commands[nomad] )); then
  complete -o nospace -C /usr/local/bin/nomad nomad
fi

# Consul autocompletion
if (( $+commands[consul] )); then
  complete -o nospace -C /usr/local/bin/consul consul
fi


alias gscp='gcloud compute scp'

# Load zmv tool
autoload -U zmv
alias mmv='noglob zmv -W'
