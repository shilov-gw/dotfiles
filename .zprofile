# Retrieve the GCE project, include value in zsh prompt
GCE_PROJECT=$(curl -s -H Metadata-Flavor:Google http://metadata.google.internal/computeMetadata/v1/project/project-id)
if (( ${+GCE_PROJECT} )); then
  function gce_project() {
    echo "%F{232}$GCE_PROJECT%{%f%}"
  }
  POWERLEVEL9K_CUSTOM_GCE_PROJECT="gce_project"
fi

function eth0() {
  # echo -e $(/sbin/ip route|awk '/eth0/ {print $9}'|tail -n1)
  echo -e $(curl -s eth0.shilov.io)
}
function eth1() {
  # echo -e $(/sbin/ip route|awk '/eth1/ {print $9}')
  hostname -I | cut -d " " -f 1
}
function pg() {
  ps aux | grep -v grep | grep $1
}
function mkcd() {
  # run mkdir unaliased
  "mkdir" -p $1 && cd $1
}

# fd makes finding files and directories easier
# fd (tool) is preferred if available, otherwise
# ff and fd functions are defined to provide
# backswards-compatability for older servers
# Check for path in case fd/ff funcs are already defined
if (( ! $+commands[fd] )); then
  function fd() {
    # find dirs
    find $1 -type d -name $2
  }
  function ff() {
    # find files
    find $1 -type f -name $2
  }
fi

function gssh() {
  cmd="gcloud compute ssh $1"
  if (( ${#2[@]} )); then
    cmd="$cmd --zone $2"
  fi
  if (( ${#3[@]} )); then
    cmd="$cmd --project $3"
  fi
  eval $cmd
}

# History search
if (( $+commands[rg] )); then
  function hs() {
    if (( ${#2[@]} )); then
      fc -il 1 | rg --no-line-number $1 | rg --no-line-number $2
    else
      fc -il 1 | rg --no-line-number $1
    fi
  }
else
  function hs() {
    if (( ${#2[@]} )); then
      fc -il 1 | grep $1 | grep $2
    else
      fc -il 1 | grep $1
    fi
  }
fi
