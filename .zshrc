source ~/.zgen/zgen.zsh
if ! zgen saved; then
  zgen oh-my-zsh
  zgen oh-my-zsh plugins/git
  zgen oh-my-zsh plugins/docker
  zgen oh-my-zsh plugins/tmux
  zgen oh-my-zsh plugins/sudo
  zgen oh-my-zsh plugins/extract
  zgen oh-my-zsh plugins/systemd
  zgen oh-my-zsh plugins/last-working-dir
  zgen oh-my-zsh plugins/systemadmin
  zgen load littleq0903/gcloud-zsh-completion
  zgen load zsh-users/zsh-syntax-highlighting
  zgen load zsh-users/zsh-completions
  zgen load zsh-users/zsh-autosuggestions
  zgen load bhilburn/powerlevel9k powerlevel9k
  zgen save
fi

# Configure prompt
if [[ $(whoami) == 'root' ]]; then DEFAULT_COLOR='016'; fi
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir time)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status background_jobs ip custom_gce_project)

# Enable badass zsh pattern matching
setopt extendedglob
# Include timestamp, duration for cmd history
setopt extended_history
# Add cmd to history before executing
setopt inc_append_history
# Share cmd history across sessions
setopt share_history
# Increase cmd history limit
HISTSIZE=50000
SAVEHIST=50000

# Show dots when waiting for autocomplete
expand-or-complete-with-dots() {
  echo -n "..."
  zle expand-or-complete
  zle redisplay
}
zle -N expand-or-complete-with-dots
bindkey "^I" expand-or-complete-with-dots
