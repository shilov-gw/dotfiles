source-file ~/.tmuxline.conf

# Enable mouse support (if applicable)
# set -g mouse on
# set -g mouse-utf8 on
# bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
# bind -n WheelDownPane select-pane -t= \; send-keys -M
# bind -n C-WheelUpPane select-pane -t= \; copy-mode -e \; send-keys -M
# bind -t vi-copy    C-WheelUpPane   halfpage-up
# bind -t vi-copy    C-WheelDownPane halfpage-down
# bind -T copy-mode-vi C-WheelUpPane   send -X halfpage-up
# bind -T copy-mode-vi C-WheelDownPane send -X halfpage-down

# Remap prefix to Control + h
set -g prefix C-h
unbind %
bind \ split-window -h
bind - split-window -v
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R
bind-key h select-pane -L
# Quick pane cycling
unbind ^N
bind ^N select-pane -t :.+
# Quick window cycling
bind C-h last-window
# Quick pane cycling
bind C-t last-pane
# Join or Send pane from/to another window
bind-key J choose-window "join-pane -h -s "%%""
bind-key S choose-window "join-pane -h -t "%%""
# Send commands to nested tmux sessions
bind-key a send-prefix
# Previous window
bind-key C-p previous-window

# Enable ctrl-left and ctrl-right for jumping between words
set-window-option -g xterm-keys on

# Use Vim bindings for copy/paste
setw -g mode-keys vi
bind [ copy-mode
bind -t vi-copy v begin-selection
bind -t vi-copy y copy-selection
bind -t vi-copy V rectangle-toggle
# For tmux 2.4+
# bind-key -T copy-mode-vi v send -X begin-selection
# bind-key -T copy-mode-vi y send -X copy-selection
# bind-key -T copy-mode-vi V send -X rectangle-toggle
bind p paste-buffer
bind ] paste-buffer

# Window should only constrained in size if a smaller client is actively looking at it
setw -g aggressive-resize on

# Theme
# Without this, colors won't be rendered correctly in apps like vim
set -g default-terminal "xterm-256color"

# Set default color for borders
set -g pane-border-fg colour244
# Highlight border of current pane
set -g pane-active-border-fg colour2

# Activity
setw -g monitor-activity on
set -g visual-activity off

# Disable autotitle
set-option -g allow-rename off

# Set history limit to 10k
set-option -g history-limit 10000

bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind c new-window -c "#{pane_current_path}"

# set -g @plugin 'tmux-plugins/tpm'
# set -g @plugin 'tmux-plugins/tmux-resurrect'
# run '~/.tmux/plugins/tpm/tpm'
# set -g @resurrect-strategy-vim 'session'

set -sg escape-time 0
