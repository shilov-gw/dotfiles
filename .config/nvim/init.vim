scriptencoding utf-8
set encoding=utf-8

set nocompatible " Turn off vi-compatibility (necessary for lots of things)

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'vim-scripts/mru.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'Raimondi/delimitMate'
Plug 'chriskempson/base16-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ekalinin/Dockerfile.vim'
Plug 'Matt-Deacalion/vim-systemd-syntax'
Plug 'terryma/vim-multiple-cursors'
Plug 'fatih/vim-hclfmt' " Note: requires github.com/fatih/hclfmt
Plug 'hashivim/vim-hashicorp-tools'
Plug 'triglav/vim-visual-increment'
call plug#end()

" Automatically open Taglist when opening vim
" let Tlist_Auto_Open = 1
" Tell vim to remember certain things when we exit
" "  '10 : marks will be remembered for up to 10 previously edited files
" "  "100 : will save up to 100 lines for each register
" "  :20 : up to 20 lines of command-line history will be remembered
" "  % : saves and restores the buffer list
" "  n... : where to save the viminfo files
set viminfo='30,\"100,:30,%,n~/.nviminfo
" Remember up to 1000 commands (default is 20)
set history=1000

set showcmd " Show full command

" set shell=/bin/zsh

set wildmenu " Better command-line completion
set wildignore=*.dll,*.o,*.obj,*.bak,*.exe,*.pyc,\*.jpg,*.gif,*.png " ignore these file formats

set ignorecase " Ignore case when searching
set smartcase " Except for capitalized searches:
set wildignorecase " Ignore case when opening files

set cursorline " Highlight the current line

set showmatch " Jump to matching brace/parenthesis/bracket

set incsearch " Start searching as I start typing

set title " Show title of the file in the titlebar

set noerrorbells " Don't beep

set novisualbell " Don't flash

set laststatus=2

" make backspace more flexible
set backspace=indent,eol,start

" Share clipboard with operating system
" set clipboard=unnamedplus

" set autochdir
autocmd BufEnter * silent! lcd %:p:h

let mapleader=" " " remap leader to space

"
" --- [ navigation ] ------------------------------------------------------------------------
"

 " Show line numbers
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" Allow buffer to be hidden if it's been modified
set hidden
" Ask for confirmation when closing unsaved buffers
set confirm
" Open a new empty buffer
nmap <leader>T :enew<cr>
" Move to the next buffer
nmap <leader>n :bnext<CR>
" Move to the previous buffer
nmap <leader>h :bprevious<CR>
" Close the current buffer and move to the previous one
nmap <leader>bq :bp <BAR> bd #<CR>
" Show all open buffers and their statuses
nmap <leader>bl :ls<CR>

" ctrl-p

let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn|tmp)|(_vendor|node_modules|vendor))$',
\}

nmap <leader>P :CtrlP<cr>        " open ctrlp (default mode)
nmap <leader>bb :CtrlPBuffer<cr> " search in buffers
nmap <leader>bs :CtrlPMRU<cr>    " search in buffers, MRU
nmap <leader>bm :CtrlPMixed<cr>  " search in buffers, files, MRU list


"
" --- [ FORMATTING ] ------------------------------------------------------------------------
"

" Smart indentation
set autoindent
" set smartindent
set smarttab

set tabstop=2
set shiftwidth=2
set expandtab

" Indenting for code blocks
set cinwords=if,else,while,do,for,switch,case,with

" Stop auto-indenting comments at the beginning of a line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

let g:hcl_fmt_autosave = 0
let g:tf_fmt_autosave = 0
let g:nomad_fmt_autosave = 0

let g:terraform_fmt_on_save = 1
let g:terraform_align=1
autocmd FileType terraform setlocal commentstring=#%s
autocmd FileType nomad setlocal commentstring=#\ %s

" Show spelling mistakes
" Use z= to show suggestions for correct spelling
nnoremap <silent> <Leader>S :set spell!<CR>

" Visual shifting (without exiting visual mode)
vnoremap < <gv
vnoremap > >gv

"
" --- [ REMAPPINGS ] ------------------------------------------------------------------------
"

" Don't timeout on mappings, only keycodes
set notimeout
set timeoutlen=500
set ttimeoutlen=50

imap jj <Esc>
noremap <Leader>s :w<CR>

" Remap gf to always open file under cursor in a new tab
" TODO temp:
" nnoremap <silent>gf <C-w>gf
" vnoremap <silent>gf <C-w>gf

" Go to the next character visually because we have word-wrap enabled
map j gj
map k gk
map $ g$
map 0 g0

" Make Y behave like other capitals
map Y y$

" Force Saving Files that Require Root Permission
cmap w!! %!sudo tee > /dev/null %

" Move previous yank/delete into register, ex: \ma (move to register a)
noremap <Leader>ma :let @a=@<CR>

"
" --- [ NAVIGATING ] ------------------------------------------------------------------------
"

set iskeyword=_,$,@,% " none of these are word dividers

set scrolloff=3 " Pad the scrolling area with 3 lines

"
" --- [ SEARCHING ] ------------------------------------------------------------------------
"

" Turn off highlighting after search
set hlsearch
nnoremap <silent> <Delete> :nohlsearch<Bar>:echo<CR>

" Use sane regexes
nnoremap / /\v
vnoremap / /\v

" Hit * to highlight every occurrence of the word under the cursor
" This keeps the cursor in place instead of jumping to next occurrence
nnoremap <F9> :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

" Search for visually selected text
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>

"
" --- [ misc ] ------------------------------------------------------------------------
"

set noswapfile
set undofile
set undodir=~/.vim/undo

" Open last buffer
noremap <Leader><Leader> <C-^>

" Enable mouse scroll
set mouse=a

" Provide a way to exit multi cursor select mode
let g:multi_cursor_quit_key = 'z'

"
" --- [ eye candy ] ------------------------------------------------------------------------
"

" Set font
" set gfn=Inconsolata-dz\ for\ Powerline:h12 " probably not necessary
let g:airline_powerline_fonts = 1
" let g:airline_theme='base16'
let g:airline_theme='murmur'

" Always show the tab bar
set showtabline=2

syntax on " Enable syntax highlighting
set background=dark
set termguicolors
let base16colorspace=256
silent! colorscheme base16-materia

" Show buffers at the top if no tabs are open
let g:airline#extensions#tabline#enabled = 1
" Show just the file name instead of the full path
let g:airline#extensions#tabline#fnamemod = ':t'

"
" --- [ HELPERS ] ------------------------------------------------------------------------
"

" Remove trailing whitespace upon save
function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction
autocmd BufWritePre *.rb,*.coffee,*.styl,*.toml :call <SID>StripTrailingWhitespaces()

set list
set listchars=tab:•\ ,trail:-,extends:•,precedes:•,nbsp:+

" Open ctags in new tab
nnoremap <silent><Leader><C-]> <C-w><C-]><C-w>T
vnoremap <silent><Leader><C-]> <C-w><C-]><C-w>T

set pastetoggle=<F2>
nnoremap <leader>p p`[v`]=`

" Remap Q (normally ex mode) to:
"   pastetoggle to disable indentation for pasted code
"   paste from system clipboard
"   pastetoggle to revert to normal behavior
map Q <F2><D-v><F2>

"
" --- [ splitting ] ------------------------------------------------------------------------
"

" Split new panes to right and bottom because the default behavior sucks
set splitbelow
set splitright
